#!/usr/bin/env ruby

require 'open-uri'
require 'zlib'

lists = Dir.glob("/var/lib/apt/lists/**/*")
re = /deb (\[arch=(.*?)\] )?(https?:\/\/.*?) (.*?) (.*?)( |$)/
out = {}
dpkg = `dpkg -l`
machine_arch = `dpkg --print-architecture`.strip
Dir.glob("/etc/apt/sources.list.d/*.list").sort.each do |file|
  lines = File.read(file).lines.grep(re)
  print "."
  if lines.any?
    matches = lines[0].match(re)
    disabled = lines[0][0] == "#"
    _, _, arch, baseuri, dist, component = matches.to_a
    dists = ["bionic", "artful", "xenial"]
    begin
      uri = "#{baseuri}/dists/#{dist}/#{component}/binary-#{arch || machine_arch}/Packages.gz"
      release_z = open(uri).read
      packages = Zlib.gunzip(release_z).lines.grep(/Package:/).map {|v| v.match(/Package: (.*?)\n/)[1] }.uniq
      packages_with_install_status = packages.select {|pkg| dpkg.lines.grep(/ii  #{pkg} /) }
      out[file] = {disabled: disabled, packages: packages_with_install_status}
    rescue OpenURI::HTTPError
      if dists.any?
        dist = dists.shift
        retry
      else
        puts "Couldn't open #{uri}"
      end
    end
  else
    puts "Unable to find anything useful in #{file}"
  end
end

GREEN = "\e[32m"
YELLOW = "\e[33m"
RED = "\e[31m"
puts "\n\nThe following sources need attention:"
out.each do |file, args|
  color = GREEN
  color = YELLOW if args[:disabled]
  color = RED if args[:packages].empty?

  if args[:disabled] || args[:packages].empty?
    puts "%s%s%s\e[0m" % [color, args[:disabled] ? "[Disabled] " : "", file]
    puts "\t" + args[:packages].join(", ") if args[:packages].any?
    puts "\t[No packages]" if args[:packages].empty?
    puts ""
  end
end
