# Source List Checker

When you upgrade your Ubuntu install, all your PPAs for older releases get
disabled. This is good, but it leaves you in a state where you might not get
updates that you'd expect to.

This tool will iterate over the source lists in your `/etc/apt.d/sources.list.d`,
find any that are disabled, and then see which packages they support which are
installed in your system.

You can then re-up your PPA subscriptions for each relevant repository to make
sure you're getting updates for your new release.

Just run it with:

    ./check.rb

## Requirements

Just any old version of ruby 2.0+.
